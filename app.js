const express = require('express');
const passport = require('passport');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const authRoutes = require('./routes/auth');
const dashboardRoutes = require('./routes/dashboard');
const app = express();
const keys = require('./config/keys')
const productRoutes = require('./routes/product')
const reviewRoutes = require('./routes/review')
const Product = require('./models/Product')
const Instance = require('./models/ProductInstance')
const Review = require('./models/Review')


mongoose.connect(keys.mongoUri)
    .then(() => {
        console.log('Mongo is ready');
    })
    .catch((e) => {
        console.log(e);
    })

app.use(passport.initialize())
require('./other/passport')(passport);
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/api/admin/auth', authRoutes);
app.use('/api/admin/dashboard', dashboardRoutes);
app.use('/api', productRoutes);
app.use('/api', reviewRoutes)

module.exports = app;


