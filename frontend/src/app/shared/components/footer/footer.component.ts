import {Component, OnInit} from '@angular/core';
import {NavigationPath} from "../../navigation.interface";

@Component({
  selector: 'app-footer', templateUrl: './footer.component.html', styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {
  NavigationPath = NavigationPath;

  constructor() {
  }

  ngOnInit(): void {
  }

}
