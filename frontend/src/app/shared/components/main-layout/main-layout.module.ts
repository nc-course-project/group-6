import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {GuestSharedModule} from "../../guest-shared.module";
import {MainLayoutComponent} from "./main-layout.component";
import {HideMenuComponent} from "../hide-menu/hide-menu.component";
import {HeaderComponent} from "../header/header.component";
import {FooterComponent} from "../footer/footer.component";


@NgModule({
  declarations: [
    MainLayoutComponent,
    HideMenuComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    GuestSharedModule,
    RouterModule,
  ],
  exports: [RouterModule],
})
export class MainLayoutModule {
}
