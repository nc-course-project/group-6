import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {GuestSharedModule} from "../../shared/guest-shared.module";
import {LikedProductsComponent} from "./liked-products/liked-products.component";

@NgModule({
  declarations: [
    LikedProductsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GuestSharedModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '', component: LikedProductsComponent,
      }

    ]),
  ],
  exports: [RouterModule],
})
export class LikedModule {
}
