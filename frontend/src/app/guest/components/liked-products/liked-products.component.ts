import {Component, OnDestroy, OnInit} from '@angular/core';
import {LikedService} from "../../services/liked.service";
import {Subscription} from "rxjs";
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-liked-products',
  templateUrl: './liked-products.component.html',
  styleUrls: ['./liked-products.component.less']
})
export class LikedProductsComponent implements OnInit, OnDestroy {
  searchKey = '';
  public favoriteProducts: any = [];
  public subscription: Subscription | undefined;

  constructor(private likedService: LikedService, private cartService: CartService) {
  }

  ngOnInit(): void {
    this.updateProducts();
    console.log(this.favoriteProducts);
  }

  updateProducts() {
    this.subscription = this.likedService.getProducts()
      .subscribe((res: any) => {
        this.favoriteProducts = res;
        this.favoriteProducts.map((product: any) => {
          console.log(product);
          product.isFavorite = true;
          this.checkCart(product);
        })
        this.cartService.searchItem.subscribe((value: any) => {
          this.searchKey = value;
        })
      })

  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  addToCart(product: any) {
    this.cartService.addToCart(product);
    product.isAddToCart = true;
    console.log(product);
  }

  addToFavorites(product: any, event: any) {
    event.stopPropagation();
    this.likedService.addToFavorites(product);
  }

  incrementQuantity(product: any) {
    this.cartService.incrementQuantity(product);
  }

  decrementQuantity(product: any) {
    if (product.quantity > 1) {
      this.cartService.decrementQuantity(product);
    } else if (product.quantity <= 1) {
      product.isAddToCart = false;
      this.cartService.removeCartItem(product);
    }
  }

  checkCart(product: any) {
    let item = this.cartService.getCartItem(product);
    if (item) {
      product.isAddToCart = true;
      product.quantity = item.quantity
    } else {
      product.isAddToCart = false;
    }
  }
}
