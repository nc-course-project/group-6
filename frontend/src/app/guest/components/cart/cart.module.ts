import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {CartComponent} from "./cart.component";
import {OrderingComponent} from "../ordering/ordering.component";
import {GuestSharedModule} from "../../../shared/guest-shared.module";

@NgModule({
  declarations: [CartComponent, OrderingComponent], imports: [CommonModule, GuestSharedModule, RouterModule.forChild([{
    path: '', component: CartComponent,
  }, {
    path: 'order', component: OrderingComponent,
  }

  ]),], exports: [RouterModule],
})
export class CartModule {
}
