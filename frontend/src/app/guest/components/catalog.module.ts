import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {ProductComponent} from "./product/product.component";
import {ProductsComponent} from "./products/products.component";
import {WidgetComponent} from "./widget/widget.component";
import {GuestSharedModule} from "../../shared/guest-shared.module";

@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent,
    WidgetComponent,
  ],
  imports: [
    CommonModule,
    GuestSharedModule,
    RouterModule.forChild([
      {
        path: '', component: ProductsComponent,
      },
      {
        path: ':id',
        component: ProductComponent
      }
    ])
  ],
  exports: [RouterModule],
})
export class CatalogModule {
}
