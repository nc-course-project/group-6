import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {CartService} from "../../services/cart.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {LikedService} from "../../services/liked.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NavigationPath} from "../../../shared/navigation.interface";


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.less']
})
export class ProductsComponent implements OnInit, OnDestroy {

  NavigationPath = NavigationPath;
  form: FormGroup | any;
  public preloader = true;
  public loading = false;
  public minPrice = 0;
  public isMore = true;
  public isLoad = false;
  public isLimit: any;
  public maxPrice = 10000;
  public limit = 1;
  public step = 8;
  public showMore = true;
  public productList: any;
  public products: any;
  public categoryList: any = [];
  public filterCategory: any = [];
  public filter: string = '';
  searchKey = '';
  public subscription: Subscription | undefined;

  constructor(private apiService: ApiService,
              private cartService: CartService,
              private likedService: LikedService,
              private route: ActivatedRoute,
              private router: Router,
  ) {

  }

  ngOnInit(): void {
    this.form = new FormGroup({
      max: new FormControl(null, [
        Validators.pattern(/^[0-9]+(?!.)/),
      ]),
      min: new FormControl(null, [
        Validators.pattern(/^[0-9]+(?!.)/),
      ]),
      sort: new FormControl('increase')

    });

    this.updateProducts();

  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  updateProducts() {
    this.loading = true;
    this.subscription = this.apiService.getProducts()
      .subscribe(res => {
        this.products = res.map((p: any) => {
          return {
            ...p,
            quantity: 1,
            total: p.price,
            isAddToCart: false,
            isFavorite: false,
          }
        });
        this.loading = false;
        this.preloader = false;
        this.sliceProducts();
        if (this.form.get('sort').value) {
          this.sortProducts();
        }
        this.filterCategory = this.productList.filter((product: any) => {
          return product.price >= this.minPrice && product.price <= this.maxPrice
        });

        /*this.route.queryParams.pipe(take(1))
          .subscribe(params => {
              console.log(params);
              if (params.filter) {
                this.filter = params.filter;
                this.filterCategory = this.productList.filter((product: any) => {
                  return product.product.category.trim() === this.filter.trim();
                });
              } else {
                this.filter = '';
              }
            }
          );*/

        this.productList.map((product: any) => {
          let item = this.cartService.getCartItem(product);
          let favoriteItem = this.likedService.getFavoriteItem(product);
          if (item) {
            product.quantity = item.quantity;
            product.isAddToCart = true;
          }
          if (favoriteItem) {
            product.isFavorite = true;
          }
          this.filterCategoryList({
            ...product,
            quantity: 1,
            total: product.price,
            isAddToCart: false,
            isFavorite: true,
          })
        })
        //
        // this.productList.map((product: any) => {
        //          this.filterCategoryList({
        //             ...product,
        //             quantity: 1,
        //             total: product.price,
        //             isAddToCart: false,
        //             isFavorite: false,
        //           })
        // })

        this.cartService.searchItem.subscribe((value: any) => {
          this.searchKey = value;
        })

        if (this.apiService.activeCategory) {
          this.filterByCategory(this.apiService.activeCategory);
        }

      });
  }

  sliceProducts() {
    this.productList = this.products.slice(0, this.limit * this.step);
  }

  loadMore() {
    if (this.isLoad) {
      this.sliceProducts();
      this.isLoad = false;
    } else {
      this.isLimit = this.products.length - 6 * this.limit;
      this.limit++;
      this.sliceProducts();
      this.updateProducts();
      if (this.isLimit > 8) {
        this.isMore = true;
      } else {
        this.isMore = false;
      }
    }
  }

  incrementQuantity(product: any) {
    this.cartService.incrementQuantity(product);
  }

  decrementQuantity(product: any) {
    if (product.quantity > 1) {
      this.cartService.decrementQuantity(product);
    } else if (product.quantity <= 1) {
      product.isAddToCart = false;
      this.cartService.removeCartItem(product);
    }
  }

  addToCart(product: any) {
    this.cartService.addToCart(product);
    product.isAddToCart = true;
  }

  filterCategoryList(product: any) {
    if (!this.categoryList.includes(product.product.category)) {
      this.categoryList.push(product.product.category);
    }
  }

  filterByGender(genderValue: any) {
    this.filterCategory = this.productList.filter((product: any) => {
      if (product.product.gender === genderValue) {
        return product;
      }
    });
  }

  filterByCategory(category: any) {
    this.filterCategory = this.productList.filter(
      (product: any) => {
        if (product.product.category === category || category === '') {
          return product;
        }
      }
    )
    if (category) {
      this.router.navigate(['catalog'], {queryParams: {filter: category}/*, queryParamsHandling: 'merge' */});
    } else {
      this.router.navigate(['catalog']);
    }
  }

  setMinPrice(event: any) {
    if (!event.target.value) {
      this.minPrice = 0;
    } else {
      this.minPrice = event.target.value;
    }
    this.updateProducts();
  }

  setMaxPrice(event
                :
                any
  ) {
    if (!event.target.value) {
      this.maxPrice = 100000;
    } else {
      this.maxPrice = event.target.value;
    }
    this.updateProducts();
  }

  sortProducts()
    :
    void {
    let products = this.productList;
    switch (this.form.get('sort').value
      ) {
      case
      "increase"
      : {
        products = products.sort((low: any, high: any) => low.price - high.price);
        this.filterCategory = products;
        break;
      }
      case
      "decrease"
      : {
        products = products.sort((low: any, high: any) => high.price - low.price);
        this.filterCategory = products;
        break;
      }
      case
      "alphabetically"
      : {
        products = products.sort((low: any, high: any) => {
          low.product.title = low.product.title.trim();
          high.product.title = high.product.title.trim();
          if (low.product.title < high.product.title) {
            return -1;
          } else if (low.product.title > high.product.title) {
            return 1;
          } else {
            return 0;
          }
        });
        this.filterCategory = products;
        break;
      }
      default: {
        products = products.sort((low: any, high: any) => low.price - high.price);
        this.filterCategory = products;
        break;
      }
    }
  }

  addToFavorites(product: any, event: any) {
    event.stopPropagation();
    this.likedService.addToFavorites(product);
    product.isFavorite = !product.isFavorite;
  }
}
