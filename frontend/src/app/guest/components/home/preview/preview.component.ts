import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-preview', templateUrl: './preview.component.html', styleUrls: ['./preview.component.less']
})
export class PreviewComponent implements OnInit {
  public selectedPreview: number = 0;
  public previews = [{
    title: 'Каждый видит мир по-своему',
    description: 'Высококачественные солнцезащитные очки, медицинские оправы и линзы от ведущих мировых производителей',
    url: 'assets/images/preview/main-bg.jpg'
  }, {
    title: 'Примерьте перед покупкой',
    description: 'Мы постоянно добавляем новые модели в онлайн примерочную – заходите чаще и следите за новинками',
    url: 'assets/images/preview/main-bg.jpg'

  }, {
    title: 'Гарантия на очки 365 дней',
    description: 'Все производители представленных у нас брендов дают свою заводскую гарантию. Гарантия распространяется на все оправы',
    url: 'assets/images/preview/main-bg.jpg'
  },];

  constructor() {
  }

  ngOnInit(): void {
  }

  public previous(): void {
    this.selectedPreview = this.selectedPreview > 0 ? this.selectedPreview - 1 : this.previews.length - 1;
    this.blur();
  }

  public next(): void {
    this.selectedPreview = this.selectedPreview < this.previews.length - 1 ? this.selectedPreview + 1 : 0;
    this.blur();
  }

  private blur(): void {
    const activeElement = document.activeElement as HTMLElement;
    if (activeElement !== null) {
      activeElement.blur()
    }
  }
}
