import {Component, OnInit} from '@angular/core';

declare const L: any;

@Component({
  selector: 'app-map', templateUrl: './map.component.html', styleUrls: ['./map.component.less']
})
export class MapComponent implements OnInit {

  public latitude = 53.1955;
  public longitude = 50.102179;
  private accessToken = 'pk.eyJ1IjoiaWx5YW1hdHRzIiwiYSI6ImNrenZvOG04ZzAwZ2YycW1ueGRwam1jamwifQ.oJQPDXMjADZQwX5zrWfJiQ';

  constructor() {
  }

  ngOnInit(): void {

    let map = L.map('map').setView([this.latitude, this.longitude], 15);

    L.tileLayer(`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${this.accessToken}`, {
      attribution: 'Магазин оптики TTTT',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'your.mapbox.access.token'
    }).addTo(map);

    let marker = L.marker([this.latitude, this.longitude]).addTo(map);

    marker.bindPopup("<h2>Магазин оптики TTTT</h2><hr/><p>г. Самара пл. Куйбышева</p><hr/><h3>Часы работы:</h3><p>пн-сб с 9:00 до 20:00 по мск</p>").openPopup();
  }

}
