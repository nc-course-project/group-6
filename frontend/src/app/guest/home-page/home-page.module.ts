import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {AboutComponent} from "../components/home/about/about.component";
import {PromoComponent} from "../components/home/promo/promo.component";
import {PopularComponent} from "../components/home/popular/popular.component";
import {ReviewsComponent} from "../components/home/reviews/reviews.component";
import {CatalogNavbarComponent} from "../components/home/catalog-navbar/catalog-navbar.component";
import {HomePageComponent} from "./home-page.component";
import {PreviewComponent} from "../components/home/preview/preview.component";
import {MapComponent} from "../components/map/map.component";
import {AboutCompanyComponent} from "../components/about-company/about-company.component";
import {ContactsComponent} from "../components/contacts/contacts.component";
import {FittingComponent} from "../components/fitting/fitting.component";
import {GuestSharedModule} from "../../shared/guest-shared.module";


@NgModule({
  declarations: [
    HomePageComponent,
    AboutComponent,
    PromoComponent,
    PopularComponent,
    ReviewsComponent,
    CatalogNavbarComponent,
    PreviewComponent,
    MapComponent,
    AboutCompanyComponent,
    ContactsComponent,
    FittingComponent,
  ],
  imports: [
    CommonModule,
    GuestSharedModule,
    RouterModule.forChild([
      {
        path: '', component: HomePageComponent,
      },
      {
        path: 'about-company', component: AboutCompanyComponent,
      },
      {
        path: 'contacts', component: ContactsComponent,
      },
      {
        path: 'fitting', component: FittingComponent,
      },
    ])
  ],
  exports: [RouterModule],
})
export class HomePageModule {
}
