import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LikedService {

  public likedItemList: any = [];
  public likedProductsList: any = new BehaviorSubject<any>([]);


  constructor() {
    const ls = JSON.parse(localStorage.getItem('favorites')!) || [];
    if (ls) {
      this.likedProductsList.next(ls);
      this.likedItemList = ls;
    } else {
      localStorage.setItem('favorites', JSON.stringify(this.likedItemList));
    }
  }

  getProducts() {
    return this.likedProductsList.asObservable();
  }

  addToFavorites(product: any) {
    const ls = JSON.parse(localStorage.getItem('favorites')!) || [];
    this.likedItemList = ls;
    let alreadyExistInFavorites = false;
    let existingFavoritesItem = undefined;
    if (this.likedItemList.length > 0) {
      existingFavoritesItem = this.likedItemList.find((el: any) => el._id === product._id);
      alreadyExistInFavorites = (existingFavoritesItem != undefined);
    }
    if (alreadyExistInFavorites) {
      this.removeFavoritesItem(product);
      return;
    } else {
      if (ls) {
        const newData = [...ls, product];
        localStorage.setItem('favorites', JSON.stringify(newData));
        this.likedItemList.push(product);
        this.likedProductsList.next(this.likedItemList);
      } else {
        this.likedItemList.push(product);
        localStorage.setItem('favorites', JSON.stringify(this.likedItemList));
        this.likedProductsList.next(this.likedItemList);
      }
    }
  }

  removeFavoritesItem(product: any) {
    const ls = JSON.parse(localStorage.getItem('favorites')!);
    this.likedItemList = ls;
    this.likedItemList.map((item: any, index: any) => {
      if (product._id === item._id) {
        this.likedItemList.splice(index, 1);
      }
    })
    localStorage.setItem('favorites', JSON.stringify(this.likedItemList));
    this.likedProductsList.next(this.likedItemList);
  }

  getFavoriteItem(product: any) {
    const ls = JSON.parse(localStorage.getItem('favorites')!) || [];
    let list = ls;
    return list.find((item: any) => {
      console.log('idCompare', item._id, product._id);
      if (item._id === product._id) {
        return item;
      }
    })

  }
}
