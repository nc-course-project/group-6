import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public activeCategory: string = '';

  constructor(private http: HttpClient) {
  }

  getProducts() {
    return this.http.get<any>('/api/products')
      .pipe(map((res: any) => {
        return res;
      }))

  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(`/api/products/${id}`)
      .pipe(map((res: any) => {
        return res//.find((p:any) => p._id === id);
      }))
  }

  getReviews() {
    return this.http.get<any>('/api/reviews')
      .pipe(map((res: any) => {
        return res;
      }))
  }

}
