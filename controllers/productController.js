const Product = require('../models/ProductInstance');

module.exports.getAll = async function (req, res) {
    const product = await Product.find({}, function (err, products) {
        if (err) return console.log(err);
        res.send(products)
    })
        .populate('product');
}
module.exports.getByID = async function (req, res) {
    const product = await Product.findById(req.params.id, function (err, products) {
        if (err) return console.log(err);
        res.send(products)
    })
        .populate('product');
}
