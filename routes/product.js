const express = require('express');
const router = express.Router();
const controller = require('../controllers/productController');

router.get('/products', controller.getAll);
router.get('/products/:id', controller.getByID)

module.exports = router;