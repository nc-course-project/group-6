const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const instance = new Schema({
    price: Number,
    count: Number,
    product: {
        ref: 'products',
        type: Schema.Types.ObjectId,
    },
    versionKey: false
})
module.exports = mongoose.model("instances", instance)